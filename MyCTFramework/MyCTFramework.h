//
//  MyCTFramework.h
//  MyCTFramework
//
//  Created by Li, Hailin on 12/24/19.
//  Copyright © 2019 lihl. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MyCTFramework.
FOUNDATION_EXPORT double MyCTFrameworkVersionNumber;

//! Project version string for MyCTFramework.
FOUNDATION_EXPORT const unsigned char MyCTFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MyCTFramework/PublicHeader.h>



//
//  MyCTFramework.swift
//  MyCTFramework
//
//  Created by Li, Hailin on 12/24/19.
//  Copyright © 2019 lihl. All rights reserved.
//

import Foundation

public class MyCTFramework: NSObject {
    
    private override init() {
        super.init()
    }
    
    public class func yourName(name: String) {
        consolLog(name: name)
    }
    
    // private func
    class func consolLog(name: String) {
        print("*********************")
        print("Welcome \(name)!!!")
        print("*********************")
    }
}
